#include <stdio.h>
#include <unistd.h>
#include <mpi.h>
#define TAMVET 5

int main(int argc, char **argv) {
    int rank, nprocs, i, j;
    int recebido[TAMVET], enviado[TAMVET];
    int vetor[TAMVET];

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    if (rank == 0) {
        for (i=1; i<nprocs; i++) {
            for (j=0; j<TAMVET; j++) {
                vetor[j]=j*3+i;
            }
            MPI_Send(vetor, TAMVET, MPI_INT, i, 1, MPI_COMM_WORLD);
        }
    } else {
        MPI_Recv(vetor, TAMVET, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Rank[%d] recebeu ", rank);
        for (i=0; i<TAMVET; i++) {
            printf("%d ", vetor[i]);
        }
        printf("\n");
    }
    MPI_Finalize();
}

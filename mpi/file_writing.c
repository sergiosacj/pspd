#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
  MPI_Init(NULL, NULL);

  // Get the number of processes and the rank of the current process
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // Open the file for writing
  MPI_File file;
  MPI_File_open(MPI_COMM_WORLD, "output.txt", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &file);

  // Write the rank of the current process to the file
  MPI_File_write_at_all(file, world_rank * sizeof(int), &world_rank, 1, MPI_INT, MPI_STATUS_IGNORE);

  // Close the file
  MPI_File_close(&file);

  MPI_Finalize();
  return 0;
}


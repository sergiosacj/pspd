// gcc -o nomeBinario -fopenmp nomeArquivo.c
#include <omp.h>
#include <stdio.h>
#define MAX 10

int main(){

    #pragma omp parallel
    {
        FILE *fd = fopen("aleatorio.in", 'r');
        int desloc = (MAX / omp_get_num_threads()) * 9;
        fseek(fd, desloc, SEEK_SET);
        int lido;
        fscanf(fd, "%d", &lido);
        fprintf(stdout, "Thread[%d] leu %d\n", omp_get_thread_num(), &lido);
    }
        return 0;
}

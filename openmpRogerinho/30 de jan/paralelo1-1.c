// gcc -o nomeBinario -fopenmp nomeArquivo.c
#include <omp.h>
#include <stdio.h>
#define MAX 100000

int main(){
    int n = 3;
    int lido;
    int sumn = 0;
	int v[MAX];
	for (int i = 0; i < MAX; i++)
    {
        scanf("%d", &v[i]);
    }


	#pragma omp prarallel for reduction (+ : sumn) private(lido)
    for (int i = 0; i < MAX; i++)
    {
        if(v[i] == n)
            sumn++;
    }
    printf("%d ocorrencias de %d\n", sumn, n);
    return 0;
}

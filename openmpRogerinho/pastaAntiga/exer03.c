// gcc nomeArquivo.c -fopenmp -o nomeBinario  
//omp_set_num_threads(6); OU pode-se usar "$ export OMP_NUM_THREADS=6" no terminal


// SERIAL MODE
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 100000000

int main(){
	int *v = malloc(sizeof(int)*MAX);
	int quant_zeros_geral = 0;
	int quant_zeros_local = 0;

	for (int i = 0; i < MAX; i++)
		if(rand()%11==0)
			v[i] = 0;
		else
			v[i] = rand();

	#pragma omp parallel private(quant_zeros_local)
	{
		quant_zeros_local = 0;
		#pragma omp for
			for (int i = 0; i < MAX; i++)
				if(v[i] == 0)
					quant_zeros_local++;
		#pragma omp atomic
			quant_zeros_geral += quant_zeros_local;
	}
	printf("o vetor possui %d zeros\n", quant_zeros_geral);
	return 0;
}

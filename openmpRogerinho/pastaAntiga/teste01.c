// gcc -o nomeBinario -fopenmp nomeArquivo.c
#include <omp.h>
#include <stdio.h>

int main(){
	omp_set_num_threads(6); // pode-se usar "$ export OMP_NUM_THREADS=6" no terminal
	printf("olá mundo\n");  //uma vez
	
	#pragma omp parallel
	{
		for(int i=0; i<3; i++)
			printf("Thread [%d] executa interação %d\n", omp_get_thread_num(), i);  //dezoito vezes (3 do for vezes 6 das threads	
	
		#pragma omp for
		for(int i=0; i<3; i++)
			printf("DENTRO DO FOR: Thread [%d] executa interação %d\n", omp_get_thread_num(), i);  // 3 vezes pois o "#pragma omp for" distribui cada interação para uma thread 	
	
	
	}
	return 0;
}

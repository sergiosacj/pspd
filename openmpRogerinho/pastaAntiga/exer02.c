// gcc nomeArquivo.c -fopenmp -o nomeBinario  
//omp_set_num_threads(6); OU pode-se usar "$ export OMP_NUM_THREADS=6" no terminal
#include <omp.h>
#include <stdio.h>

int main(){
	//omp_set_num_threads(6); // pode-se usar "$ export OMP_NUM_THREADS=6" no terminal
	int vetor[omp_get_max_threads()];
	for (int i = 0; i < omp_get_max_threads(); i++)
		vetor[i] = 0;
	#pragma omp parallel
	{
		#pragma omp for
		for (int i = 0; i < 1000; i++){
			vetor[omp_get_thread_num()]++;
			}
	}
	for(int i = 0; i<omp_get_max_threads(); i++){
		printf("thread = %d rodou %d vezes\n", i, vetor[i]);
	}
	return 0;
}

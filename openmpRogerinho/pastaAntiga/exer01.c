// gcc -o nomeBinario -fopenmp nomeArquivo.c
#include <omp.h>
#include <stdio.h>

int main(){
	omp_set_num_threads(6); // pode-se usar "$ export OMP_NUM_THREADS=6" no terminal
	#pragma omp parallel
	{
		for(int i=0; i<3; i++)printf("olá mundo\n");  //uma vez	
	}
	return 0;
}

// compilar => mpicc exercicio_com_n_workers.c -o exercicio_com_n_workers
// rodar    => mpirun -n 4 ./exercicio_com_n_workers
// Rodrigo Tiago Costa Lima 	180037242
// Sergio de Almeida Cipriano Junior 180037439
#include <mpi.h>
#include <stdio.h>
#include <sys/types.h>
#define TAMVET 16

int main(int argc, char **argv) {
  int meurank, nprocs;
  int A[TAMVET];
  int B[TAMVET];
  int vrecebido1[TAMVET / 2], vrecebido2[TAMVET / 2], total[TAMVET];
  int i, j, x = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &meurank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  if (meurank == 0) {
    for (i = 0; i < TAMVET; i++) {
      A[i] = i;
      B[i] = i;
    }
    // nprocs-1 pois enviamos um i a frente
    for (i = 0; i < nprocs - 1; i++) {
      int start = TAMVET / (nprocs - 1) * i;
      int size = TAMVET / (nprocs - 1);
      if (i == nprocs - 2 && nprocs % 2 == 0)
        size += 1;
      MPI_Send(&A[start], size, MPI_INT, i + 1, 0, MPI_COMM_WORLD);
      MPI_Send(&B[start], size, MPI_INT, i + 1, 0, MPI_COMM_WORLD);
    }
    for (i = 1; i < nprocs; i++) {
      int size = TAMVET / (nprocs - 1);
      if (i == nprocs - 1 && nprocs % 2 == 0)
        size += 1;
      MPI_Recv(vrecebido1, size, MPI_INT, i, 0, MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);
      for (j = 0; j < size; j++)
        printf("%d ", vrecebido1[j]);
      printf("\n");
    }

  } else {
    int size = TAMVET / (nprocs - 1);
    if (meurank == nprocs - 1 && nprocs % 2 == 0)
      size += 1;
    MPI_Recv(vrecebido1, size, MPI_INT, 0, 0, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
    MPI_Recv(vrecebido2, size, MPI_INT, 0, 0, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
    for (i = 0; i < size; i++)
      total[i] = vrecebido1[i] + vrecebido2[i];
    MPI_Send(total, size, MPI_INT, 0, 0, MPI_COMM_WORLD);
  }

  MPI_Finalize();
  return 0;
}

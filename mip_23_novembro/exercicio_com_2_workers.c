// Rodrigo Tiago Costa Lima 	180037242
// Sergio de Almeida Cipriano Junior 180037439
#include <mpi.h>
#include <stdio.h>
#include <sys/types.h>
#define TAMVET 16

int main(int argc, char **argv) {
  int meurank, nprocs;
  int A[TAMVET];
  int B[TAMVET];
  int vrecebido1[TAMVET / 2], vrecebido2[TAMVET / 2], total[TAMVET / 2];
  int i, j, x = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &meurank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  if (meurank == 0) {
    for (i = 0; i < TAMVET; i++) {
      A[i] = i;
      B[i] = i;
    }
    MPI_Send(A, TAMVET / 2, MPI_INT, 1, 0, MPI_COMM_WORLD);
    MPI_Send(&A[TAMVET / 2], TAMVET / 2, MPI_INT, 2, 0, MPI_COMM_WORLD);
    MPI_Send(B, TAMVET / 2, MPI_INT, 1, 0, MPI_COMM_WORLD);
    MPI_Send(&B[TAMVET / 2], TAMVET / 2, MPI_INT, 2, 0, MPI_COMM_WORLD);

    MPI_Recv(vrecebido1, TAMVET, MPI_INT, 1, 0, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
    MPI_Recv(vrecebido2, TAMVET, MPI_INT, 2, 0, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
    for (i = 0; i < TAMVET / 2; i++)
      printf("%d ", vrecebido1[i]);
    for (i = 0; i < TAMVET / 2; i++)
      printf("%d ", vrecebido2[i]);
    printf("\n");

  } else {
    MPI_Recv(vrecebido1, TAMVET / 2, MPI_INT, 0, 0, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
    MPI_Recv(vrecebido2, TAMVET / 2, MPI_INT, 0, 0, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
    for (i = 0; i < TAMVET / 2; i++)
      total[i] = vrecebido1[i] + vrecebido2[i];
    MPI_Send(total, TAMVET / 2, MPI_INT, 0, 0, MPI_COMM_WORLD);
  }

  MPI_Finalize();
  return 0;
}

MPI -> sistemas de memória distribuída
-  Máquinas diferentes (N hosts)

OpenMP -> sistemas de memória compartilhada
- N processadores/cores
- N unidades vetorias (#pragma vector)

PROVA:
a) Comunicação em Sistemas Distribuídos
b) Programação paralela/distribuída com OpenMP e MPI
c) Redes P2P, blockchain, algoritmos de consenso
d) Clusters e ambientes de programação distribuída/paralela (Hadoop, Spark)

#include <stdio.h>
#include <sys/types.h>
#include <mpi.h>
#define TAMVET 5

int main(int argc, char ** argv){
	int meurank, nprocs;
	int vetor[TAMVET], vetenvio[TAMVET], vrecebido[TAMVET];
	int i, j

	for(i=0; i<TAMVET; i++){
		vetor[i]=i+3;
	}


	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meurank);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

	if(meurank == 0){
		for(i=1; i<nprocs; i++){
			for(j=0; j<TAMVET; j++){
				vetenvio[j] = vetor[j] * i;
			}	
			MPI_Send(vetnvio, TAMVET, MPI_INT, i, 0, MPI_COMM_WORLD);
			printf("Master enviando %d para %d\n", venvio, i);
		}	/////////////////////////////
	}
	else{
		MPI_Recv(vrecebido, TAMVET, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf("RANK[%d] recebeu %d\n", meurank, vrecebido);
	}

	MPI_Finalize();
	return 0;
}

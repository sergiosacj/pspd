#include <stdio.h>
#include <mpi.h>
#include <sys/types.h>
#include <string.h>

//compilar = mpicc mpi01.c -o mpi01 
//rodar = mpirun -n 4 ./mpi01

int main(int argc, char * argv[]){
	int meurank, nprocs;
	char msg[100];
	MPI_Status status;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meurank);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);


	printf("Olá mundo! Meu pid= %d e meu rank = %d\n", getpid(), meurank);

	if(meurank != 0){
	//estou nos slaves...
		sprintf(msg, "I'm alive");
		MPI_Send(msg, strlen(msg)+1, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
			//mensegm, tamanho, tipo, rankDoRecebedor, tag inteira, qual o grupo
	}

	else{
	//estou na master...
		for(int i=1; i<nprocs;i++){
			MPI_Recv(msg, 100, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
				//mensagem, tamanho máximo, tipo, de quem, qual tag, grupo, status
			printf("processo >%d< mensagem>%s<\n", status.MPI_SOURCE, msg);
		}

	}
	MPI_Finalize();
	return 0;
}

#include <stdio.h>
#include <string.h>
char texto[2100000000];
long long int counter[3];

int main(int argc, char **argv) {
  if (argc != 2) {
    fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
    return 1;
  }
  FILE *file = fopen(argv[1], "r");
  while (fgets(texto, 2100000000, file)) {
    char delimit[] = " \n";
    char *word = strtok(texto, delimit);
    while (word != NULL) {
      word[strcspn(word, "\n")] = 0;
      if (strlen(word) < 6)
        counter[0]++;
      else if (strlen(word) >= 6 && strlen(word) <= 10)
        counter[1]++;
      counter[2]++;
      word = strtok(NULL, delimit);
    }
  }
  printf("arquivo contem %lld palavras com menos de 6 letras\n", counter[0]);

  printf(
      "arquivo contem %lld palavras com pelo menos 6 letras e até 10 letras\n",
      counter[1]);

  printf("arquivo contem %lld palavras\n", counter[2]);
  fclose(file);
  return 0;
}

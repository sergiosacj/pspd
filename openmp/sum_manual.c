#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <string.h>

#define MAX 10000000

int main ()
{
  int *v=malloc(sizeof(int)*MAX);
  int zeros_counter=0;

  for (int i=0; i<MAX; i++)
    if (rand()%11)
      v[i] = rand();
    else
      v[i] = 0;

  int local_zeros_counter = 0;
  #pragma omp parallel private(local_zeros_counter)
  {
    #pragma omp for
    for (int i=0; i<MAX; i++)
      if (!v[i])
        local_zeros_counter++;

    #pragma omp atomic
    zeros_counter += local_zeros_counter;
  }

  printf("zeros_counter = %d\n", zeros_counter);
  return 0;
}

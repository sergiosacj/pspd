#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  int n = 8, a[n], b[n], c[n];
  int total_threads = 5;
  int n_per_thread;
  for (int i = 0; i < n; i++) {
    a[i] = i;
    b[i] = i;
  }

  omp_set_num_threads(total_threads);
  n_per_thread = n/total_threads;

  #pragma omp parallel for
  for(int i=0; i<n; i++) {
    c[i] = a[i]+b[i];
    printf("Thread %d works on element%d\n", omp_get_thread_num(), i);
  }

  printf("i\ta[i]\t+\tb[i]\t=\tc[i]\n");
  for(int i=0; i<n; i++)
    printf("%d\t%d\t\t%d\t\t%d\n", i, a[i], b[i], c[i]);
}


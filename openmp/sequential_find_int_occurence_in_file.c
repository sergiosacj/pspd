#include <stdio.h>

#define MAX 1000000

int main() {
  int n = 3, read, sum = 0;
  scanf("%d", &read);

  for (size_t i = 0; i < MAX; ++i) {
    if (read == n)
      sum++;
  }

  printf("%d ocorrências de %d\n", sum, n);
  return 0;
}

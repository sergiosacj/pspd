#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <string.h>

#define MAX 10000000

int main ()
{
  int *v=malloc(sizeof(int)*MAX);

  for (int i=0; i<MAX; i++)
    if (rand()%11)
      v[i] = rand();
    else
      v[i] = 0;

  long long i = 0, zeros_counter = 0;
  #pragma omp parallel for reduction (+ : zeros_counter)
  for (i=0; i<MAX; i++)
    zeros_counter += v[i];

  printf("zeros_counter = %lld\n", zeros_counter);
  return 0;
}

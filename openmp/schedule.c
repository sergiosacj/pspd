#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <string.h>

int main ()
{
  int n = 25, v[n];
  omp_set_num_threads(5);

  #pragma omp parallel for schedule(static)
  for (int i = 0; i<n; i++)  
    v[i] = omp_get_thread_num();

  for (int i = 0; i<n; i++)  
    printf("v[%d] = %d\n", i, v[i]);
}

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  int n = 8, a[n], b[n], c[n];
  for (int i = 0; i < n; i++) {
    a[i] = i;
    b[i] = i;
  }

  #pragma omp parallel
  {
    int id, i, nthreads, istart, iend, n_per_thread;
    nthreads = omp_get_num_threads();
    n_per_thread = n/nthreads;
    id = omp_get_thread_num();
    istart = id * n / nthreads;
    iend = (id + 1) * n / nthreads;
    if (id == nthreads - 1)
      iend = n;
    for(int i = istart; i < iend; i++)
      c[i] = a[i]+b[i];
  }

  printf("i\ta[i]\t+\tb[i]\t=\tc[i]\n");
  for(int i=0; i<n; i++)
    printf("%d\t%d\t\t%d\t\t%d\n", i, a[i], b[i], c[i]);
}
